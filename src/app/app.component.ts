import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  bodyEl: HTMLBodyElement = document.body as HTMLBodyElement;
  @ViewChild('appContainer') appContainer: ElementRef<HTMLDivElement>;
  @ViewChild('fontDropdown') fontDropdown: ElementRef<HTMLDivElement>;
  selectedTheme: 'Dark' | 'Light' = "Light";
  selectedFont: string = "Default";

  ngOnInit() {
    this.bodyEl.classList.add('light-theme');
  }

  toggleTheme() {
    const appContainerDiv = this.appContainer.nativeElement;
    if (this.selectedTheme == "Dark") {
      this.bodyEl.classList.add('light-theme');
      this.bodyEl.classList.remove('dark-theme');
      this.selectedTheme = 'Light';
    } else {
      this.bodyEl.classList.add('dark-theme');
      this.bodyEl.classList.remove('light-theme');
      this.selectedTheme = 'Dark';
    }
  }

  toggleDropdown() {
    const dropdownDiv = this.fontDropdown.nativeElement;
    if (!dropdownDiv.classList.contains('is-active')) {
      dropdownDiv.classList.add('is-active');
    } else {
      dropdownDiv.classList.remove('is-active');
    }
  }

  selectFont(fontName: string) {
    const appContainerDiv = this.appContainer.nativeElement;
    this.resetFont();
    this.selectedFont = fontName;
    switch(fontName) {
      case 'Courier': {
        appContainerDiv.classList.add('courier');
        break;
      }
      case 'Cambria': {
        appContainerDiv.classList.add('cambria');
        break;
      }
      case 'Times New Roman': {
        appContainerDiv.classList.add('times-new-roman');
        break;
      }
      case 'Lucida Sans': {
        appContainerDiv.classList.add('lucida-sans');
        break;
      }
      case 'Reset':
      default: {
        this.selectedFont = 'Default';
        break;
      }
    }
    console.log(appContainerDiv.classList.toString())
    this.toggleDropdown();
  }

  resetFont() {
    const appContainerDiv = this.appContainer.nativeElement;
    console.log(appContainerDiv.classList.toString())
    appContainerDiv.classList.remove('courier', 'cambria', 'lucida-sans', 'times-new-roman');
    console.log(appContainerDiv.classList.toString())
  }
}

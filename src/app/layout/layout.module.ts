import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContentComponent } from './content/content.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [HeaderComponent, FooterComponent, ContentComponent],
  exports: [HeaderComponent, FooterComponent, ContentComponent ],
  entryComponents: [HeaderComponent, FooterComponent, ContentComponent]
})
export class LayoutModule { }

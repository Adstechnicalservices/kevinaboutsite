import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { PageNotFoundPageComponent } from './page-not-found-page/page-not-found-page.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { HireMePageComponent } from './hire-me-page/hire-me-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AboutPageComponent, HomePageComponent, ContactPageComponent, PageNotFoundPageComponent, HireMePageComponent],
  exports: [AboutPageComponent, HomePageComponent, ContactPageComponent, PageNotFoundPageComponent, HireMePageComponent],
  entryComponents: [AboutPageComponent, HomePageComponent, ContactPageComponent, PageNotFoundPageComponent, HireMePageComponent]
})
export class PagesModule { }

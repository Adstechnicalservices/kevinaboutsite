import { Routes } from "@angular/router";
import { AboutPageComponent } from "./pages/about-page/about-page.component";
import { ContactPageComponent } from "./pages/contact-page/contact-page.component";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { PageNotFoundPageComponent } from "./pages/page-not-found-page/page-not-found-page.component";
import { HireMePageComponent } from "./pages/hire-me-page/hire-me-page.component";

export const appRoutes: Routes = [
    { path: '',   redirectTo: '/about', pathMatch: 'full' },
    { path: 'home', component: HomePageComponent },
    { path: 'about', component: AboutPageComponent },
    { path: 'hire-me', component: HireMePageComponent },
    { path: 'contact', component: ContactPageComponent },
    { path: '**', component: PageNotFoundPageComponent }
  ];